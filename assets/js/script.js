$('.variable-width').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true
});

$(".slider-1").slick({
    // dots: false,
    infinite: true,
    variableWidth: true
});
$(".slider-2").slick({
    dots: true,
    infinite: true,
    variableWidth: true
});